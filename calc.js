class Klausur {
    number;
    note;
    lp;
    einbezogen = 0;
    constructor(number, note, lp) {
        this.number = number;
        this.note = note;
        this.lp = lp;
    }
}

function calculate() {
    document.getElementById("msgERROR").style.display = "none";
    try{
    let noten = document.getElementById("txtNoten").value;

    let klausuren = [];

    for (let zeile of noten.split("\n")) {
        let splits = zeile.trim().split(" ");
        if (splits.length < 3) continue;
        let name = "";
        for (let i = 0; i < splits.length - 2; i++) {
            name += splits[i] + " ";
        }
        let note = parseFloat(splits[splits.length - 2].replaceAll(",", "."));
        let lp = parseInt(splits[splits.length - 1]);
        if(isNaN(note) || isNaN(lp)) throw Error("could not interpret Klausur: " + zeile);
        klausuren.push(new Klausur(name,note,lp));
    }

    // Klausuren aufsteigend nach Note sortieren
    klausuren.sort((a, b) => a.note - b.note);

    let lpSum = 0;
    let weightedSum = 0;

    // berechnung der Durchnittsnote mit maximal 90LP
    for (let klausur of klausuren) {
        if (lpSum >= 90) break;
        var benoteteLP = Math.min(klausur.lp, 90 - lpSum);
        klausur.einbezogen = benoteteLP;
        weightedSum += benoteteLP * klausur.note;
        lpSum += benoteteLP;
    }

    // die Note wäre dann: weightedSum / lp

    setKlausurList(klausuren)
    setResult(weightedSum, lpSum);
    
    }catch(e){
        console.error(e);
        document.getElementById("msgERROR").style.display = "flex";
    }
}

function setResult(weightedSum, lp) {
    document.getElementById("avgNote").innerText = (weightedSum / lp).toFixed(2);
    document.getElementById("lpSum").innerText = lp;
    document.getElementById("divInput").style.display = "none";

    setWorstCase(weightedSum, lp);
    
    document.getElementById("divResult").style.display = "flex";
    document.getElementById("msg" + (lp >= 90 ? "O" : "U") + "90LP").style.display = "flex";
}


function setKlausurList(klausuren) {
    let klausurenList = document.getElementById("klausurenList");
    klausurenList.style.display = "unset";

    for (let klausur of klausuren) {
        let einbezogen = "";
        let kDom = document.createElement("div");
        kDom.classList.add("klausur");
        if (klausur.einbezogen == klausur.lp) kDom.classList.add("k_einbezogen");
        else if (klausur.einbezogen > 0) {
            kDom.classList.add("k_partiell");
            einbezogen = '<div class="klausur_info"><i>(nur ' + klausur.einbezogen + ' LP einbezogen)</i></div>';
        }
        else kDom.classList.add("k_weggefallen");
        kDom.innerHTML = `
        <div class="klausur_nr">` + klausur.number + `</div> ` + einbezogen + `
        <div class="klausur_info">Note: <b>` + klausur.note.toFixed(1) + `</b></div>
        <div class="klausur_info">LP: <b>` + klausur.lp + `</b></div>
        `;
        klausurenList.appendChild(kDom);
    }
}

function setWorstCase(wSum, lp) {
    console.log(wSum + "  " + lp);
    if (lp < 90) {
        wSum += (90 - lp) * 4.0;
        lp = 90;
    }

    // add worst case bachelor thesis
    wSum += 12 * 4.0;
    lp += 12;

    document.getElementById("avgWCNote").innerText = (wSum / lp).toFixed(2);
}